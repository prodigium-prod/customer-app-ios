import React from 'react';
import { store, persistor } from './store';
import { Provider } from 'react-redux';
import { PersistGate } from 'redux-persist/integration/react';
import { NavigationContainer } from '@react-navigation/native';
import Router from './router'
import {StyleSheet, Text, View} from 'react-native';
// import OngoingOrdersNotification from './components/ongoingorder/ongoingorder'; // Import the OngoingOrdersNotification component.

const App = () => {
  console.log('tae')
  return (
      // <View style={styles.container}>
      //
      // </View>
      <Provider store={store}>
        <PersistGate loading={null} persistor={persistor}>
          <NavigationContainer>
          {/*  <View style={styles.container}>*/}
              <Router />
          {/*    /!* <OngoingOrdersNotification /> *!/*/}
          {/*  </View>*/}
          </NavigationContainer>
        </PersistGate>
      </Provider>
  );
}


const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#322',
    alignItems: 'center',
    justifyContent: 'center',
  },
});

export default App;
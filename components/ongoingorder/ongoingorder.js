import React from 'react';
import { View, Text, StyleSheet, TouchableOpacity, ImageBackground } from 'react-native';
import { useSelector } from 'react-redux';
import { LinearGradient } from 'expo-linear-gradient'; // Import LinearGradient
import { useNavigation } from '@react-navigation/native'; // Import useNavigation hook
import Icon from 'react-native-vector-icons/Ionicons';

const OngoingOrdersNotification = () => {
  const navigation = useNavigation(); // Use useNavigation hook to get navigation object
//   const [isVisible, setIsVisible] = useState(true); // State to control the visibility of the notification
  // Get the ongoing orders from the Redux store
  const ongoingOrders = useSelector(state => state.application.order);

  if (!ongoingOrders) {
    // If no ongoing orders, don't render the notification component
    return null;
  }

  const handleNotificationPress = () => {
    // Navigate to the Task screen when notification is pressed
    navigation.navigate('Task');
  };

  return (
    <TouchableOpacity style={styles.container} onPress={handleNotificationPress}>
      <LinearGradient
        colors={['#5865F2', '#DD7DE1']} // Use the same combination color
        style={styles.gradient}
      >
        <View>
          <Text style={styles.notificationText}>Order ID: {ongoingOrders.order_id}</Text>
          <Text style={styles.notificationText}>Address: {ongoingOrders.address}</Text>
          {/* Display other relevant order details */}
        </View>
      </LinearGradient>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  container: {
    position: 'absolute',
    bottom: 10,
    left: 30,
    right: 30,
    zIndex: 999, // Ensure the notification is above other components
    marginBottom: 80
  },
  gradient: {
    padding: 10,
    borderRadius: 10,
  },
  notificationText: {
    color: '#FFFFFF',
    fontSize: 16,
    fontFamily: 'YourFontFamily', // Replace with your font family
  },
});

export default OngoingOrdersNotification;

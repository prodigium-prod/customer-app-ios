import {BackHandler, Pressable, ScrollView, StyleSheet, Text, View} from "react-native";
import React, {useEffect, useState} from "react";
import {Gap} from "../../components";
import {
    DateIcon,
    DateIconGreen,
    DateIconRed,
    LocationIcon,
    LocationIconGreen,
    LocationIconRed,
    ProfileIcon,
    ProfileIconGreen,
    ProfileIconRed,
    TimeIcon,
    TimeIconGreen,
    TimeIconRed
} from "../../assets";
import {heightPercentageToDP as hp, widthPercentageToDP as wp,} from "react-native-responsive-screen";
import AsyncStorage from '@react-native-async-storage/async-storage';
import LottieView from 'lottie-react-native'
import {useSelector} from 'react-redux';
// import Pusher from 'pusher-js/react-native';


const Task = ({navigation}) => {
    useEffect(() => {
        const backAction = () => {
            // Check if the current screen is Profile, if yes, navigate to Home and return true to prevent the default back action.
            if (navigation.isFocused()) {
                navigation.navigate('Home');
                return true;
            }
            // Return false to perform the default back action (close the app if there's no previous screen).
            return false;
        };

        // Add back press listener
        BackHandler.addEventListener('hardwareBackPress', backAction);

        // Clean up the listener when the component is unmounted
        return () => {
            BackHandler.removeEventListener('hardwareBackPress', backAction);
        };
    }, [navigation]);
    const userId = useSelector(state => state.auth.login.userId);
    const [count, setCount] = useState(0);
    const [HistoryDone, setHistoryDone] = useState('');
    const [HistoryOtherDone, setHistoryOtherDone] = useState('');
    const [sortHistoryOtherDone, setSortHistoryOtherDone] = useState('');
    const [sortHistoryDone, setSortHistoryDone] = useState('');
    const [loggedIn, setLoggedIn] = useState(false);
    const [history, setHistory] = useState('')

    useEffect(() => {
        checkLoginStatus();
    }, []);

    const checkLoginStatus = async () => {
        try {
            const value = await AsyncStorage.getItem('loggedIn');
            if (value === 'true') {
                setLoggedIn(true);
            }
        } catch (error) {
            console.log('Error reading login status from AsyncStorage:', error);
        }
    };

    // const pusher = new Pusher('fe971a32f0035f3d7453', {
    //     cluster: 'ap1',
    //     forceTLS: true, // Use SSL/TLS for secure connections
    // });

    // Subscribe to the 'order-channel' with the event 'order-update'
    // const [dataChanel, setDataChanel] = useState('');
    // useEffect(() => {
    //   // const id = useSelector(state => state.auth.login.userId)
    //   async function fetchOrder() {
    //     try {
    //       console.log(userId)
    //       const response = await fetch(`https://customer.kilapin.com/order/history/${userId}`);
    //       const data = await response.json()
    //       console.log('data fetch',data)
    //       // Subscribe to the 'my-channel' with the event 'my-event'
    //         // const channel = pusher.subscribe('my-channel');
    //         // channel.bind(`${userId}`, (data) => {
    //         //   // Handle the real-time update received from Pusher
    //         //   console.log('Received event data:', data);
    //         //   setDataChanel(data); // Update the dataChanel state with the received data
    //         // });

    //     } catch (error) {
    //       console.log(error)
    //     }

    //   }
    //   fetchOrder()
    //   // Clean up the Pusher subscription when the component is unmounted
    //   return () => {
    //     pusher.unsubscribe('my-channel');
    //     pusher.disconnect();
    //   };
    // }, []);


    useEffect(() => {
        const fetchOrderStatus = async () => {
            try {
                const id = await AsyncStorage.getItem('id');
                // const link = `https://customer.kilapin.com/order/history/${id}`;
                const link = `https://customer.kilapin.com/order/on-going/${id}`;
                const response = await fetch(link);
                const data = await response.json();
                const orderHistory = data.data;
                console.log('order history', orderHistory)
                setSortHistoryOtherDone(orderHistory)
                // setOrderHistory(orderHistory);

                // const historyDone = orderHistory.filter(
                //   item => item.status === 'Done' || item.status === 'Cancel' || item.status === 'Waiting for Payment'
                // );
                // setHistoryDone(historyDone);

                // const historyOtherDone = orderHistory.filter(
                //   item => item.status !== 'Done' && item.status !== 'Waiting for Payment' && item.status !== 'Cancel'
                // );
                // setHistoryOtherDone(historyOtherDone);

                // const sortedHistoryDone = [...historyDone].sort(
                //   (a, b) => new Date(a.createdAt) - new Date(b.createdAt)
                // );
                // setSortHistoryDone(sortedHistoryDone);

                // const sortedHistoryOtherDone = [...historyOtherDone].sort(
                //   (a, b) => new Date(b.createdAt) - new Date(a.createdAt)
                // );
                // setSortHistoryOtherDone(sortedHistoryOtherDone);
                // // console.log(sortHistoryOtherDone)
            } catch (error) {
                console.log(error);
            }
        };

        fetchOrderStatus();
        const intervalId = setInterval(() => {
            setCount(count => count + 1);
        }, 3000);

        return () => clearInterval(intervalId);
    }, [count]);


    const capitalizeWords = (input) => {
        return input.split(' ').map(word => word.charAt(0).toUpperCase() + word.slice(1)).join(' ');
    };
    if (loggedIn) {
        return (
            <View style={styles.allcontainer}>
                <View style={styles.container}>
                    <Text style={styles.order}>Order</Text>
                    <Gap height={25}/>
                    <View style={styles.filtersec}>
                        <Text style={styles.filtertext}>Your Order List</Text>
                    </View>
                    <Gap height={10}/>
                    <View height='80%' flexGrow={1}>
                        <ScrollView contentContainerStyle={{
                            justifyContent: "center",
                            alignContent: "center",
                            alignItems: "center",
                        }}>
                            <View style={styles.orderhistoryscroll}>
                                <View>
                                    {(
                                        sortHistoryOtherDone
                                    ) ? (<View>
                                        <View style={[styles.ordercard, {
                                            borderColor:
                                                sortHistoryOtherDone.status === "Done" ? "#32CD32" :
                                                    sortHistoryOtherDone.status === "Canceled" ? "#FF6347" : "#54CC76"
                                        }]}>
                                            <View>
                                                <View
                                                    style={styles.topcontentcard}
                                                    onPress={() => navigation.navigate("Tracking")}
                                                    key={sortHistoryOtherDone.order_id}
                                                >
                                                    <View>
                                                        <Text
                                                            style={styles.ordertitle1}>{sortHistoryOtherDone.service} Cleaning</Text>
                                                        <Text style={styles.ordertitle2}>
                                                            {sortHistoryOtherDone.order_id}
                                                            {/* {capitalizeWords(OrderHistory.item_name.replace(/-/g, ' ').split(' ').slice(0, 16).join(' '))}... */}
                                                        </Text>
                                                    </View>
                                                    <View
                                                        style={[styles.orderstatus, {
                                                            backgroundColor:
                                                                sortHistoryOtherDone.status === "Done" ? "#32CD32" :
                                                                    sortHistoryOtherDone.status === "Canceled" ? "#FF6347" : "#54CC76"
                                                        }]}
                                                        onPress={() => navigation.navigate("Tracking", {order_id: sortHistoryOtherDone.order_id})}
                                                    >
                                                        <Text
                                                            style={styles.orderstatustext}
                                                            disabled={sortHistoryOtherDone.status === "Waiting for Payment" || sortHistoryOtherDone.status === "Done" || sortHistoryOtherDone.status === "Open" || sortHistoryOtherDone.status === "Ready to Booked"}
                                                            onPress={() => navigation.navigate(
                                                                "Tracking", {order_id: sortHistoryOtherDone.order_id}
                                                            )}

                                                        >
                                                            {(sortHistoryOtherDone.status === ("Open") || sortHistoryOtherDone.status === ("Ready to Booked")) ? ("Waiting For Cleaner") : ((sortHistoryOtherDone.status === "Placement") ? ("Get Cleaner") : (sortHistoryOtherDone.status))}
                                                        </Text>
                                                    </View>
                                                </View>
                                                <Gap height={20}/>
                                                <View style={styles.person}>
                                                    {sortHistoryOtherDone.status === "Done" ? <ProfileIcon/> :
                                                        <ProfileIconGreen/>}
                                                    <Gap width={10}/>
                                                    <Text
                                                        style={styles.persontext}>{sortHistoryOtherDone.cleaner_id ? ("Cleaner Ranger") : ("Wait!")}</Text>
                                                    <Gap width={35}/>
                                                    <View style={styles.date2}>
                                                        {sortHistoryOtherDone.status === "Done" ? <DateIcon/> :
                                                            <DateIconGreen/>}
                                                        <Gap width={10}/>
                                                        <Text style={styles.persontext}>
                                                            {/* waktu */}
                                                            {sortHistoryOtherDone.createdAt ? (sortHistoryOtherDone.createdAt.slice(0, 10)) : ("Silahkan menunggu")}
                                                        </Text>
                                                    </View>
                                                </View>
                                                <Gap height={20}/>
                                                <View style={styles.person}>
                                                    {sortHistoryOtherDone.status === "Done" ? <TimeIcon/> :
                                                        <TimeIconGreen/>}
                                                    <Gap width={10}/>
                                                    <Text style={styles.persontext}>
                                                        {sortHistoryOtherDone.booking_type === "Urgent" ? ("Now") :
                                                            (sortHistoryOtherDone.time)
                                                            // 'time'
                                                        }
                                                    </Text>
                                                    <View style={styles.date}>
                                                        <Gap width={20}/>
                                                        {sortHistoryOtherDone.status === "Done" ? <LocationIcon/> :
                                                            <LocationIconGreen/>}
                                                        <Gap width={10}/>
                                                        <Text style={{fontFamily: 'Ubuntur',}}>
                                                            {sortHistoryOtherDone.address}
                                                            {/* {OrderHistory.address.split(' ').slice(0,4).join(' ')}... */}
                                                        </Text>
                                                    </View>
                                                </View>
                                            </View>
                                        </View>
                                    </View>) : (<Text>Loading...</Text>)}
                                    {(

                                        HistoryDone
                                    ) ? (<View>
                                        {HistoryDone.map((OrderHistory) => {
                                            return (
                                                <View style={[styles.ordercard, {
                                                    borderColor:
                                                        OrderHistory.status === "Done" ? "#DA7DE1" :
                                                            OrderHistory.status === "Canceled" ? "#FF6347" : "#CC5353"
                                                }]}>
                                                    <View>
                                                        <View
                                                            style={styles.topcontentcard}
                                                            onPress={() => navigation.navigate(
                                                                "Tracking", {order_id: OrderHistory.order_id})}
                                                            key={OrderHistory.order_id}
                                                        >
                                                            <View>
                                                                <Text
                                                                    style={styles.ordertitle1}>{OrderHistory.service} Cleaning</Text>
                                                                <Text
                                                                    style={styles.ordertitle2}>{OrderHistory.item_name}</Text>
                                                            </View>
                                                            <View
                                                                style={[styles.orderstatus, {
                                                                    backgroundColor:
                                                                        OrderHistory.status === "Done" ? "#DA7DE1" :
                                                                            OrderHistory.status === "Canceled" ? "#FF6347" : "#CC5353"
                                                                }]}
                                                                onPress={() => navigation.navigate("Tracking", {order_id: OrderHistory.order_id})}
                                                            >
                                                                <Text
                                                                    style={styles.orderstatustext}
                                                                    disabled={OrderHistory.status === "Waiting for Payment" || OrderHistory.status === "Done" || OrderHistory.status === "Open" || OrderHistory.status === "Ready to Book"}
                                                                    onPress={() => navigation.navigate(
                                                                        "Tracking", {order_id: OrderHistory.order_id}
                                                                    )}>
                                                                    {(OrderHistory.status === ("Open") || OrderHistory.status === ("Ready to Book")) ? ("Waiting For Cleaner") : ((OrderHistory.status === "Waiting for Payment") ? ("Canceled") : (OrderHistory.status))}
                                                                </Text>
                                                            </View>
                                                        </View>
                                                        <Gap height={20}/>
                                                        <View style={styles.person}>
                                                            {OrderHistory.status === "Done" ? <ProfileIcon/> :
                                                                <ProfileIconRed/>}
                                                            <Gap width={10}/>
                                                            <Text
                                                                style={styles.persontext}>{OrderHistory.cleaner_id ? ("Cleaner Ranger") : ("Wait!")}</Text>
                                                            <Gap width={35}/>
                                                            <View style={styles.date2}>
                                                                {OrderHistory.status === "Done" ? <DateIcon/> :
                                                                    <DateIconRed/>}
                                                                <Gap width={10}/>
                                                                <Text
                                                                    style={styles.persontext}>{OrderHistory.createdAt}</Text>
                                                            </View>
                                                        </View>
                                                        <Gap height={20}/>
                                                        <View style={styles.person}>
                                                            {OrderHistory.status === "Done" ? <TimeIcon/> :
                                                                <TimeIconRed/>}
                                                            <Gap width={10}/>
                                                            <Text style={styles.persontext}>
                                                                {OrderHistory.booking_type === "Urgent" ? ("Now") :
                                                                    // 'service'
                                                                    (OrderHistory.time)
                                                                }
                                                            </Text>
                                                            <View style={styles.date}>
                                                                <Gap width={20}/>
                                                                {OrderHistory.status === "Done" ? <LocationIcon/> :
                                                                    <LocationIconRed/>}
                                                                <Gap width={10}/>
                                                                <Text style={{fontFamily: 'Ubuntur',}}>
                                                                    {OrderHistory.address}...
                                                                </Text>
                                                            </View>
                                                        </View>
                                                    </View>
                                                </View>
                                            )
                                        })}
                                    </View>) : (<Text>Loading...</Text>)}
                                </View>

                            </View>
                        </ScrollView>
                    </View>
                    <View style={{backgroundColor: '#fff', padding: '2%'}}></View>
                </View>
            </View>
        );
    }
    return (
        <View style={styles.containerr}>
            <View style={{marginTop: '11%'}}></View>
            <ScrollView>
                <View style={styles.containerr}>
                    <Text style={{fontFamily: 'Ubuntu', fontSize: 22,}}>Order</Text>
                    <LottieView
                        source={require('../../assets/animation/GembokLocked.json')}
                        autoPlay
                        loop
                        style={styles.namelogo}/>
                    <Text style={styles.titletext}>Silahkan login dan nikmati semua layanan!</Text>
                    <Text style={styles.subtext}>Silakan login untuk mendapatkan akses penuh dan menikmati semua layanan
                        yang tersedia dalam aplikasi Kilapin</Text>
                    <Pressable style={styles.nextbutton} onPress={() => navigation.navigate('Login')}>
                        <Text style={styles.textbutton} onPress={() => navigation.navigate('Login')}>LOGIN</Text>
                    </Pressable>
                </View>
            </ScrollView>
        </View>
    )
};


const styles = StyleSheet.create({
        container: {
            justifyContent: "center",
            alignItems: "center",
            backgroundColor: '#fff'
        },
        filtersec: {
            flexDirection: "row",
            alignItems: "center",
            justifyContent: "center",
            textAlign: "center",
        },
        filtertext: {
            fontFamily: "Ubuntum",
            fontSize: 14,
            alignItems: "center",
            justifyContent: "center",
            textAlign: "center",
            marginBottom: 15,
        },
        order: {
            fontFamily: "Ubuntu",
            fontSize: 20,
            color: "#1E2022",
            marginTop: hp('4%')
        },
        ordercard: {
            width: 320,
            height: 140,
            backgroundColor: "#FFF",
            borderColor: '#DA7DE1',
            borderWidth: 2,
            borderRadius: 15,
            marginBottom: hp('3%'),
        },
        topcontentcard: {
            alignItems: "center",
            justifyContent: "space-between",
            textAlign: "center",
            flexDirection: "row",
        },
        orderstatus: {
            alignItems: "center",
            justifyContent: "center",
            textAlign: "center",
            backgroundColor: "#DA7DE1",
            borderRadius: 20,
            width: 95,
            height: 30,
            marginRight: 12,
            marginTop: 10,
            fontSize: 12,
            marginLeft: '-3%'
        },
        orderstatustext: {
            color: "#fff",
            fontFamily: "Ubuntu",
            fontSize: 12,
        },
        ordertitle1: {
            marginTop: 12,
            marginLeft: 12,
            color: "#1E2022",
            fontSize: 12,
            fontFamily: "Ubuntu",
            marginBottom: 2
        },
        ordertitle2: {
            marginLeft: 12,
            fontSize: 10,
            fontFamily: "Ubuntur",
            width: '90%',
            marginBottom: -2
        },
        person: {
            marginLeft: 12,
            flexDirection: "row",
        },
        persontext: {
            fontFamily: "Ubuntur",
            color: "#4B4B4B",
        },
        date: {
            flexDirection: "row",
        },
        date2: {
            flexDirection: "row",
            marginLeft: '-5%'
        },
        containerr: {
            flex: 1,
            alignItems: 'center',
            justifyContent: 'center',
            backgroundColor: '#fff'
        },
        namelogo: {
            height: hp('45%'),
            marginBottom: hp('0.5%'),
            alignItems: 'center',
            justifyContent: 'center',
            textAlign: 'center',
        },
        titletext: {
            color: '#5865F2',
            fontFamily: 'Ubuntu',
            fontSize: 30,
            alignItems: 'center',
            justifyContent: 'center',
            textAlign: 'center',
            width: wp('80%'),
            marginBottom: hp('2%')
        },
        subtext: {
            fontFamily: 'Ubuntur',
            fontSize: 14,
            alignItems: 'center',
            justifyContent: 'center',
            textAlign: 'center',
            width: 300,
            marginBottom: hp('2%')
        },
        nextbutton: {
            backgroundColor: '#5865F2',
            height: 51,
            borderRadius: 30,
            width: 300,
            alignItems: 'center',
            justifyContent: 'center',
            textAlign: 'center',
            marginTop: '2%'
        },
        textbutton: {
            fontFamily: 'Ubuntu',
            color: '#fff',
        },
    }
);

export default Task;
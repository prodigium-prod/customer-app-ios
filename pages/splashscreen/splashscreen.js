import {StyleSheet, View} from 'react-native';
import React, {useEffect} from 'react';
import {useFonts} from 'expo-font';
import LottieView from 'lottie-react-native';
import {heightPercentageToDP as hp} from 'react-native-responsive-screen';
import AsyncStorage from '@react-native-async-storage/async-storage';

const Splashscreen = ({navigation}) => {
    useEffect(() => {
        const checkLoginStatus = async () => {
            const loggedIn = await AsyncStorage.getItem('loggedIn');
            if (loggedIn === 'true') {
                setTimeout(() => {
                    navigation.replace('MainApp');
                }, 500); // Add a delay of 1 second before navigating
            } else {
                setTimeout(() => {
                    navigation.replace('LanguageSelection');
                }, 500);
            }
        };

        checkLoginStatus();
    }, [navigation]);

    const [fontsLoaded] = useFonts({
        Ubuntu: require("../../assets/fonts/Ubuntu-Bold.ttf"),
        Ubuntur: require("../../assets/fonts/Ubuntu-Regular.ttf"),
        Ubuntum: require("../../assets/fonts/Ubuntu-Medium.ttf"),
    });
    if (!fontsLoaded) return null;

    return (
        <View style={styles.background}>
            {/*<LottieView*/}
            {/*    source={require('../../assets/animation/kilapinwithtagline.json')}*/}
            {/*    autoPlay*/}
            {/*    loop*/}
            {/*    style={{*/}
            {/*        height: hp('20%'),*/}
            {/*        marginTop: hp('20%')*/}
            {/*    }}*/}
            {/*/>*/}
            {/*<LottieView*/}
            {/*    source={require('../../assets/animation/poweredbyprodigium.json')}*/}
            {/*    autoPlay*/}
            {/*    loop*/}
            {/*    style={{*/}
            {/*        height: hp('5%'),*/}
            {/*        marginBottom: hp('4%')*/}
            {/*    }}*/}
            {/*/>*/}
        </View>
    );
};

const styles = StyleSheet.create({
    background: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'space-between',
        flexDirection: 'column',
        backgroundColor: '#fff'
    }
});

export default Splashscreen;
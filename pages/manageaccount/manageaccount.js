import React from 'react';
import {StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';

const ManageAccount = ({navigation}) => {

    const handleLogout = async () => {
        try {
            await AsyncStorage.removeItem('loggedIn');
            navigation.replace('MainApp');
        } catch (error) {
            console.error(error);
        }
    }


    return (<View style={styles.container}>
        <View>
            <Text style={styles.title}>Kelola Akun</Text>
        </View>

        <TouchableOpacity style={styles.button} onPress={handleLogout}>
            <Text style={styles.buttonText} onPress={handleLogout}>Log Out</Text>
        </TouchableOpacity>
    </View>);
};

const styles = StyleSheet.create({
    container: {
        flex: 1, paddingHorizontal: 20, backgroundColor: '#fff'
    }, title: {
        fontSize: 24, fontFamily: 'Ubuntu', marginBottom: '5%', marginTop: '10%', textAlign: 'center'
    }, button: {
        backgroundColor: '#5865F2', paddingVertical: 12, paddingHorizontal: 24, borderRadius: 20, marginTop: '5%',
    }, buttonText: {
        color: '#FFFFFF', fontSize: 16, fontWeight: 'bold', textAlign: 'center',
    },
});

export default ManageAccount;
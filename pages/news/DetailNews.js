import React, {useEffect, useState} from 'react';
import {Dimensions, Image, SafeAreaView, ScrollView, StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import {heightPercentageToDP as hp, widthPercentageToDP as wp,} from "react-native-responsive-screen";
import {BackIcon} from '../../assets';
import WebView from "react-native-webview";


const DetailNews = ({navigation, route}) => {
    const [news, setNews] = useState({});
    const {newsId} = route.params;
    console.log(newsId);

    const getNewsDetail = async () => {
        const response = await fetch(`https://customer.kilapin.com/news/detail/${newsId}`);
        const data = await response.json();
        console.log(data);
        setNews(data.data);
    };

    useEffect(() => {
        getNewsDetail();
    }, []);

    const wrapHTMLWithStyle = (html, fontSize) => `
    <style>
        p {
        /*word-break: break-all;*/
        white-space: pre-wrap;
        }
        body {
            font-size: ${fontSize};
        }
    </style>
    ${html}`;

    // Define the font size you want for the content in the WebView
    const desiredFontSize = '2.5rem'; // Adjust this value as needed

    return (
        <View style={styles.container}>
            <TouchableOpacity
                style={{marginTop: hp('4.9%'), marginRight: wp('6%'), width: "50%"}}
                onPress={() => navigation.navigate('News')}>
                <BackIcon style={{width: "50%"}}/>
            </TouchableOpacity>
            <SafeAreaView>

                {Object.keys(news).length > 0 ? (
                    <View>
                        <View style={{flexDirection: 'row'}}>
                            <View>
                                <Text style={styles.title}>{news.title}</Text>
                                <Text style={styles.author}>By Admin | 17 March 2023</Text>
                            </View>
                        </View>
                        <Image style={styles.headerImage} source={{uri: news.image}}/>
                        <ScrollView>
                            <View style={styles.text_render}>
                                <WebView
                                    style={styles.web_view_text}
                                    originWhitelist={['*']}
                                    source={{html: wrapHTMLWithStyle(news.desc, desiredFontSize)}}
                                />
                            </View>
                        </ScrollView>
                    </View>

                ) : (
                    <View>
                        <Text>tunggu sejenak</Text>
                    </View>
                )}


            </SafeAreaView>
        </View>

    );
};

const styles = StyleSheet.create({
    text_render: {
        flex: 1,
        flexWrap: "wrap",
        width: Dimensions.get("screen").width,
        height: Dimensions.get("screen").height / 1.5
    },
    web_view_text: {
        flexWrap: "wrap",
        width: Dimensions.get("screen").width - 40,
        height: Dimensions.get("screen").height / 1.5
    },
    container: {
        flex: 1,
        padding: 20,
        backgroundColor: 'white',
    },
    headerImage: {
        height: 200,
        width: '100%',
        marginBottom: hp('2%'),
        borderRadius: 15
    },
    title: {
        fontSize: 24,
        fontWeight: 'bold',
        marginTop: 20,
        marginBottom: 1,
    },
    author: {
        fontSize: 16,
        color: '#aaa',
        marginBottom: 20,
    },
    paragraph: {
        fontSize: 18,
        lineHeight: 30,
        marginBottom: hp('2%'),
    },
});

export default DetailNews;
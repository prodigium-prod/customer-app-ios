import {BackHandler, Dimensions, Image, ImageBackground, Pressable, StyleSheet, Text, View} from "react-native";
import React, {useEffect, useState} from "react";
import {heightPercentageToDP as hp,} from "react-native-responsive-screen";
import Swiper from 'react-native-swiper';
import {useTranslation} from 'react-i18next';
import {SafeAreaView} from "react-native-safe-area-context";

const {height} = Dimensions.get('window');
const SWIPE_THRESHOLD = height

const News = ({navigation}) => {
    useEffect(() => {
        const backAction = () => {
            // Check if the current screen is Profile, if yes, navigate to Home and return true to prevent the default back action.
            if (navigation.isFocused()) {
                navigation.navigate('Home');
                return true;
            }
            // Return false to perform the default back action (close the app if there's no previous screen).
            return false;
        };

        // Add back press listener
        BackHandler.addEventListener('hardwareBackPress', backAction);

        // Clean up the listener when the component is unmounted
        return () => {
            BackHandler.removeEventListener('hardwareBackPress', backAction);
        };
    }, [navigation]);
    const [newsData, setNewsData] = useState('');
    const {t} = useTranslation();

    const fetchData = async () => {
        const response = await fetch(`https://customer.kilapin.com/news/`);
        const data = await response.json();
        console.log(data);
        setNewsData(data.data);
    };

    useEffect(() => {
        fetchData()
    }, [])

    return (
        <View style={styles.container}>
            <View style={styles.section_swipper}>
                <Swiper autoplay={true} autoplayTimeout={5} paginationStyle={{marginBottom: '0%'}}>
                    <ImageBackground
                        source={require("../../assets/image/F45T.png")}
                        resizeMode="cover"
                        style={styles.image_slider}>
                    </ImageBackground>

                    <ImageBackground
                        source={require("../../assets/image/diskon.png")}
                        resizeMode="cover"
                        style={styles.image_slider}
                    >
                    </ImageBackground>

                    <ImageBackground
                        source={require("../../assets/image/carousel3.png")}
                        resizeMode="cover"
                        style={styles.image_slider}>
                    </ImageBackground>
                </Swiper>
            </View>

            <SafeAreaView>
                <View style={styles.section_list_news}>
                    <View style={styles.toptext}>
                        <Text style={styles.news}>{t('news_promo')}</Text>
                        <Text style={styles.newsseeall}>Kilapin</Text>
                    </View>
                    {newsData ?
                        <View style={{height: '100%', flex: 1, flexDirection: 'row', flexWrap: 'wrap', marginTop: 10}}>
                            {newsData.map((news) => {
                                return (
                                    <View style={{
                                        width: '50%',
                                        height: '60%',
                                        borderRadius: 20,
                                        padding: '2%',
                                        justifyContent: "center"
                                    }}
                                          key={news._id}
                                    >
                                        <Pressable
                                            onPress={() => navigation.navigate("DetailNews", {newsId: news._id})}
                                        >
                                            <View
                                                style={{height: '100%', borderRadius: 20,}}
                                            >
                                                <Image
                                                    style={{width: '100%', height: '100%', borderRadius: 20,}}
                                                    source={{uri: news.image}}
                                                />
                                            </View>
                                            <View style={styles.news_text}>
                                                <Text style={{
                                                    fontSize: 15,
                                                    textAlign: "center",
                                                    marginTop: 6,
                                                }}>{news.title}</Text>
                                            </View>
                                        </Pressable>
                                    </View>
                                )
                            })}
                        </View> : <View></View>}
                </View>
            </SafeAreaView>
        </View>

    );
};

const styles = StyleSheet.create({
    section_list_news: {
        width: Dimensions.get("screen").width,
        height: "60%"
    },
    image_slider: {
        width: "100%",
        height: "100%"
    },
    container: {
        backgroundColor: "white",
        flex: 1,
        width: Dimensions.get("screen").width,
        height: Dimensions.get("screen").height
    },
    section_swipper: {
        backgroundColor: "white",
        flex: 1,
        width: "100%",
        height: "100%"
    },
    news: {
        fontFamily: "Ubuntu",
        fontSize: 20,
    },
    news_text: {
        fontSize: 20
    },
    newsseeall: {
        fontFamily: "Ubuntum",
        color: "#5865F2",
        fontSize: 20,
    },
    toptext: {
        flexDirection: "row",
        justifyContent: "space-between",
        marginBottom: '2%',
        paddingHorizontal: '7%'
    },
    namelogo: {
        flex: 1,
        color: "#fff",
        fontFamily: "Ubuntu",
        fontSize: 32,
        height: hp("25%"),
        marginTop: hp("20%"),
    },
    shape: {
        alignItems: "center",
        justifyContent: "center",
        textAlign: "center",
        alignSelf: "stretch",
        // height: '100%'
    },
});

export default News;
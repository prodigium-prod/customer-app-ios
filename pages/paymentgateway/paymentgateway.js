import React, {useState} from 'react';
import {Linking, Pressable, StyleSheet, Text, View} from 'react-native';
import {heightPercentageToDP as hp, widthPercentageToDP as wp} from 'react-native-responsive-screen';
import AsyncStorage from '@react-native-async-storage/async-storage';
import LottieView from 'lottie-react-native'
import {useSelector} from 'react-redux';


const RadioButton = () => {
    // const address = useSelector(state => state.application.address);
    const service = useSelector(state => state.application.size);
    const addOn = useSelector(state => state.application.floor);
    const voucherDiscount = useSelector(state => state.application.discount)
    const order_detail = useSelector(state => state.application.order);
    console.log("hasil redux", order_detail)
    const address = order_detail.address
    const gross_amount = order_detail.total_price
    const [url, setUrl] = useState('')
    AsyncStorage.getItem('url').then((res) => setUrl(res))
    console.log(url)
    return (
        <View style={styles.container}>
            <LottieView
                source={require('../../assets/animation/bill.json')}
                autoPlay
                loop
                style={{
                    height: hp('45%'),
                    marginBottom: hp('15%')
                }}/>
            <View style={{marginTop: hp('-10%'),}}></View>
            <View style={styles.shapebill2}>
                <View style={styles.shapebill}>
                    <View style={{justifyContent: 'center', alignItems: 'center', textAlign: 'center'}}>
                        <Text style={styles.pricetext}>Detail Pembayaran</Text>
                        <View style={{
                            flexDirection: 'row',
                            marginTop: '2.5%',
                            justifyContent: 'flex-start',
                            alignItems: 'flex-start'
                        }}>
                            <Text style={{color: '#fff', marginRight: '3%'}}>Jenis Layanan:</Text>
                            <Text style={{color: '#fff', width: '40%'}}>Apartment {service} General Cleaning</Text>
                        </View>
                        <View
                            style={{flexDirection: 'row', marginTop: '1%', marginBottom: '0.6%', marginLeft: '-1.3%'}}>
                            <Text style={{color: '#fff', marginRight: '3%'}}>Alamat:</Text>
                            <Text style={{
                                color: '#fff',
                                width: '50%',
                                marginHorizontal: 'auto'
                            }}>{address ? address.substring(0, 45) : 'tunggu =ya'}...</Text>
                        </View>
                        <View>
                            <View style={{
                                flexDirection: 'row',
                                justifyContent: 'flex-start',
                                alignItems: 'flex-start',
                                textAlign: 'left',
                                marginTop: '1%'
                            }}>
                                <Text style={{color: '#fff', marginRight: '3%'}}>Harga Pokok:</Text>
                                <Text style={{color: '#fff'}}>Rp.{gross_amount}</Text>
                            </View>
                            <View style={{
                                flexDirection: 'row',
                                justifyContent: 'flex-start',
                                alignItems: 'center',
                                textAlign: 'left',
                                marginTop: '2%'
                            }}>
                                <Text style={{color: '#fff', marginRight: '3%'}}>Add-ons:</Text>
                                <Text style={{color: '#fff', width: '50%'}}>{addOn}</Text>
                            </View>
                            <View style={{
                                flexDirection: 'row',
                                justifyContent: 'flex-start',
                                alignItems: 'center',
                                textAlign: 'center',
                                marginTop: '2%'
                            }}>
                                <Text style={{color: '#fff', marginRight: '3%'}}>Diskon:</Text>
                                <Text
                                    style={{color: '#fff'}}>{voucherDiscount ? (`Rp. ${(gross_amount * (100 / voucherDiscount) - (gross_amount))}`) : `-`}</Text>
                            </View>
                        </View>
                    </View>
                </View>
            </View>
        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#fff',
        width: wp('100%'),
        height: hp('100%')
    },
    shapebill: {
        backgroundColor: '#DA7DE1',
        alignItems: 'flex-start',
        justifyContent: 'flex-start',
        borderRadius: 20,
        paddingBottom: hp('6.5%'),
        marginTop: '-3%',
        marginBottom: '5%'
    },
    shapebill2: {
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 20,
    },
    pricetext: {
        fontFamily: 'Ubuntu',
        fontSize: 28,
        width: 300,
        alignItems: 'center',
        justifyContent: 'center',
        textAlign: 'center',
        color: '#fff',
        marginTop: hp('2%')
    },
    descp: {
        width: wp('70%'),
        alignItems: 'center',
        justifyContent: 'center',
        textAlign: 'center',
        marginTop: hp('0.5%'),
        color: '#fff'
    },
    nextbutton: {
        backgroundColor: '#DA7DE1',
        height: 45,
        borderRadius: 30,
        width: '40%',
        alignItems: 'center',
        justifyContent: 'center',
        textAlign: 'center',
        marginTop: 20,
        color: '#fff',
        fontFamily: 'Ubuntu',
        marginRight: '2.5%',
        marginBottom: '5.4%'
    },
    nextbutton2: {
        backgroundColor: '#353535',
        height: 45,
        borderRadius: 1130,
        width: '40%',
        alignItems: 'center',
        justifyContent: 'center',
        textAlign: 'center',
        color: '#fff',
        fontFamily: 'Ubuntu',
    },
    textbutton2: {
        fontFamily: 'Ubuntu',
        color: '#fff',
        fontSize: 12
    },
    textbutton: {
        fontFamily: 'Ubuntu',
        color: '#fff',
    },
});

export default function App({navigation}) {

    const [url, setUrl] = useState('');

    AsyncStorage.getItem('url').then((res) => setUrl(res));
    console.log(url);

    return (
        <View style={{flex: 1, justifyContent: 'center', alignItems: 'center', backgroundColor: '#fff'}}>
            <RadioButton/>
            <View style={{
                flexDirection: 'row',
                justifyContent: 'center',
                alignItems: 'center',
                marginTop: '-5%',
                marginBottom: '5%',
                marginRight: '0.5%'
            }}>
                <Pressable style={styles.nextbutton} onPress={() => Linking.openURL(`${url}`)}>
                    <Text style={styles.textbutton}>BAYAR DISINI</Text>
                </Pressable>
                <Pressable style={styles.nextbutton2} onPress={() => navigation.navigate('PaymentSuccessful')}>
                    <Text style={styles.textbutton2}>SAYA SUDAH BAYAR</Text>
                </Pressable>
            </View>
        </View>
    );
}
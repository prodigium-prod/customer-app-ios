import { StyleSheet, View, Text, Pressable } from 'react-native'
import React, { useEffect,useState } from 'react'
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import LottieView from 'lottie-react-native'
import AsyncStorage from '@react-native-async-storage/async-storage';
// import {useDispatch, useSelector} from 'react-redux';
// import {ApplicationActions} from '@actions';

const Paymentsuccessful = ({navigation}) => {
    // const dispatch = useDispatch();
    const [OrderStatus, setOrderStatus] = useState('');
    const [pesan, setPesan] = useState('')
    // dispatch(ApplicationActions.onclearVoucher());


    useEffect(() => {
  
      const fetchOrderStatus = async () => {
        try {
            console.log("menjalankan get data order")
            var order_id = await AsyncStorage.getItem('order_id')
            console.log(order_id)
            const link = `https://customer.kilapin.com/notif/status/${order_id}`
            const response = await fetch(link);
            const data = await response.json()
            console.log("message",order_id,data)
            if (data.message) {
                if (data.message === "Sudah membayar"){
                    setPesan("Yeay! Your cleaner is on the way!")
                } else if (data.message === "pembayaran expired") {
                    setPesan("Your payment expired, please re-order!")
                } else {
                    setPesan("Please pay your order")
                }
            } else {
                setPesan("Loading")
            }
            console.log(pesan)
        } catch (error) {
          
        }  
      }
      fetchOrderStatus()
    },[])

  return (
    <View style={styles.container}>
    <LottieView 
        source={require('../../assets/animation/ontheway.json')}
        autoPlay
        loop
        style={styles.animation}/>
      <Text style={styles.titletext}>{pesan ? (pesan):("Loading")}</Text>
      <Text style={styles.subtext}>Selamat cleaning service terbaik akan segera tiba dan membuat tempat kamu menjadi sangat bersih!</Text>
      <Pressable style={styles.nextbutton} onPress={() => {pesan === "Yeay! Your cleaner is on the way!" ? (navigation.navigate('Tutorial')):(navigation.navigate('PaymentGateway'))}}>
            <Text style={styles.textbutton} onPress={() => {pesan === "Yeay! Your cleaner is on the way!" ? (navigation.navigate('Tutorial')):(navigation.navigate('PaymentGateway'))}}>NEXT</Text>
        </Pressable>
    </View>
  )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#fff'
    },
    animation: {
        height: hp('40%'),
        marginTop: hp('5%'),
        marginBottom: hp('5%')
    },
    titletext: {
        color: '#4552AF',
        fontFamily: 'Ubuntu',
        fontSize: 30,
        alignItems: 'center',
        justifyContent: 'center',
        textAlign: 'center',
        width: wp('80%'),
        marginBottom: hp('2%'),
        marginTop: hp('5%')
    },
    subtext: {
        fontFamily: 'Ubuntur',
        fontSize: 14,
        alignItems: 'center',
        justifyContent: 'center',
        textAlign: 'center',
        width: 300,
        marginBottom: hp('2%'),
        color: '#343434'
    },
    nextbutton: {
        backgroundColor: '#4552AF',
        height: 51,
        borderRadius:30,
        width: 300,
        alignItems: 'center',
        justifyContent: 'center',
        textAlign: 'center',
        marginTop: hp('15%')
    },
    textbutton: {
        fontFamily: 'Ubuntu',
        color: '#fff',
    },
})

export default Paymentsuccessful
import React, {useEffect, useState} from 'react';
import {
    BackHandler,
    Dimensions,
    KeyboardAvoidingView,
    Platform,
    SafeAreaView,
    StyleSheet,
    Text,
    TextInput,
    TouchableOpacity,
    View,
} from 'react-native';
import {BackIcon} from '../../assets';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {Gap} from '../../components'
import {useTranslation} from 'react-i18next';
import {useDispatch, useSelector} from 'react-redux';
import {ApplicationActions} from '../../actions';
import RNDateTimePicker from '@react-native-community/datetimepicker';
import SearchableDropdown from 'react-native-searchable-dropdown';


const {width, height} = Dimensions.get('window');

const ASPECT_RATIO = width / height;
const LATITUDE_DELTA = 0.0922;
const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO;

// const SearchableDropdown = ({
//                                 onTextChange,
//                                 onItemSelect,
//                                 containerStyle,
//                                 itemStyle,
//                                 itemsContainerStyle,
//                                 items,
//                                 defaultIndex,
//                                 resetValue,
//                                 underlineColorAndroid,
//                                 placeholder,
//                             }) => {
//     const [searchText, setSearchText] = useState('');
//     const [filteredItems, setFilteredItems] = useState(items);
//
//     const handleTextChange = (text) => {
//         setSearchText(text);
//         const filteredItems = items.filter((item) =>
//             item.label.toLowerCase().includes(text.toLowerCase())
//         );
//         setFilteredItems(filteredItems);
//         onTextChange(text);
//     };
//
//     return (
//         <View style={containerStyle}>
//             <TextInput
//                 style={styles.input}
//                 onChangeText={handleTextChange}
//                 value={searchText}
//                 underlineColorAndroid={underlineColorAndroid}
//                 placeholder={placeholder}
//             />
//             <FlatList
//                 data={filteredItems}
//                 keyExtractor={(item) => item.value}
//                 renderItem={({item}) => (
//                     <TouchableOpacity
//                         style={itemStyle}
//                         onPress={() => {
//                             setSearchText(item.label);
//                             setFilteredItems(items);
//                             onItemSelect(item);
//                         }}
//                     >
//                         <Text>{item.label}</Text>
//                     </TouchableOpacity>
//                 )}
//                 contentContainerStyle={itemsContainerStyle}
//             />
//         </View>
//     );
// };


const CustomRadioGroup = ({options, selectedOption, onSelect}) => {
    const [selectedId, setSelectedId] = useState(selectedOption);

    const handleRadioPress = (id) => {
        setSelectedId(id);
        onSelect(id);
    };

    const createRows = () => {
        const rows = [];
        for (let i = 0; i < options.length; i += 2) {
            const rowOptions = options.slice(i, i + 2);
            const row = (
                <View key={i} style={styles.containerSelectOption}>
                    {rowOptions.map((option) => (
                        <TouchableOpacity
                            key={option.id}
                            style={[
                                styles.radioOption,
                                selectedId === option.id ? styles.selectedRadio : null,
                            ]}
                            onPress={() => handleRadioPress(option.id)}
                        >
                            <Text style={styles.optionLabel}>{option.label}</Text>
                        </TouchableOpacity>
                    ))}
                </View>
            );
            rows.push(row);
        }
        return rows;
    };

    return <View>{createRows()}</View>;

};


const Order = ({navigation, route}) => {
    const {t} = useTranslation();

    // const [codeVoucher, setCodeVoucher] = useState('');
    // const [discout,setDiscount] = useState('')
    const codeRedux = useSelector(state => state.application.code)
    const voucherDiscount = useSelector(state => state.application.discount)
    console.log("codeRedux", codeRedux, voucherDiscount)
    let new_time = useSelector(state => state.application.time);
    const userId = useSelector(state => state.auth.login.userId);
    const [addressInput, setAddressInput] = useState('');
    const [selectedAddress, setSelectedAddress] = useState(null);
    const dispatch = useDispatch();
    const booking_type = useSelector(state => state.application.orderType)
    const description = useSelector(state => state.application.address);
    const postal_code = useSelector(state => state.application.postalcode);
    const voucherlist = useSelector(state => state.application.voucher);
    const orderType = useSelector(state => state.application.orderType);

    const [isOpen, setIsOpen] = useState(false);
    const [isOpenAddOns, setIsOpenAddOns] = useState(false);
    const [isGraniteChecked, setGraniteChecked] = useState(false);
    const [isMarbleChecked, setMarbleChecked] = useState(false)
    const [isInsuranceChecked, setInsuranceChecked] = useState(false);
    const [expanded, setExpanded] = useState(false);
    const [selectedVoucher, setSelectedVoucher] = useState(null);
    const [voucherData, setVoucherData] = useState([]);
    const [selectedHour, setSelectedHour] = useState('00');
    const [selectedMinute, setSelectedMinute] = useState('00');

    const hours = Array.from({length: 25}, (_, index) => index.toString().padStart(2, '0'));
    const minutes = Array.from({length: 60}, (_, index) => index.toString().padStart(2, '0'));

    const [date, setDate] = useState(new Date())

    const handleHourChange = (hour) => {
        setSelectedHour(hour);
        handleTimeChange(`${hour}:${selectedMinute}`);
    };

    const handleMinuteChange = (minute) => {
        setSelectedMinute(minute);
        handleTimeChange(`${selectedHour}:${minute}`);
    };

    const getSelectedAddOns = () => {
        const selectedAddOns = [];
        if (isGraniteChecked) {
            selectedAddOns.push('Lantai Granite');
        }
        if (isMarbleChecked) {
            selectedAddOns.push('Lantai Marble');
        }
        if (isInsuranceChecked) {
            selectedAddOns.push('Asuransi');
        }
        return selectedAddOns;
    };

    const selectedAddOns = getSelectedAddOns();

    const filterVoucherByZone = (voucherlist, description) => {
        if (!Array.isArray(voucherlist) || typeof description !== 'string') {
            // Handle invalid input gracefully
            return [];
        }

        const parsedDescription = description.toLowerCase();

        // Filter the voucherlist based on the zone property
        return voucherlist.filter((voucher) => {
            const zone = voucher.zone;
            if (typeof zone === 'string') {
                return zone.toLowerCase().includes(parsedDescription);
            }
            return false; // Skip vouchers without a valid zone property
        });
    };

    const filteredVouchers = filterVoucherByZone(voucherlist, description);
    console.log("Filtered Vouchers: ", filteredVouchers);


    useEffect(() => {
        const backAction = () => {
            navigation.goBack();
            return true;
        };

        const backHandler = BackHandler.addEventListener('hardwareBackPress', backAction);

        return () => backHandler.remove();
    }, [navigation]);

    const [data, setMemberData] = useState('');
    const [address, setAddress] = useState('')
    const [notes, setNotes] = useState('')
    const [voucher, setVoucher] = useState('')
    // const [service, setService] = useState('Urgent')

    const handleVoucherPress = () => {
        dispatch(ApplicationActions.onsizeOrder(item_name))
        dispatch(ApplicationActions.ontimeOrder(time))
        navigation.navigate('Voucher', {page: 'Order'});
    };

    const handleSelectVoucher = (value) => {
        const selected = filteredVouchers.find((voucher) => voucher.id === value);
        setSelectedVoucher(selected);
        setIsOpen(false);
    };

    useEffect(() => {
        console.log('time', time)
        console.log("ini postal code", postal_code)
        console.log('option dispatch', booking_type)
        const fetchMemberData = async () => {
            try {
                console.log("menjalankan get data user")
                const id = await AsyncStorage.getItem('id')
                const link = `https://customer.kilapin.com/users/${id}`
                const response = await fetch(link);
                const dataMember = await response.json()

                setMemberData(dataMember.data)
                if (response.ok) {
                    console.log("data diri sudah ready", dataMember)
                }
            } catch (error) {
                console.log(error)
            }
        }
        fetchMemberData()
    }, [])

    const [item_name, setSelectedPackage] = useState('');
    const [time, setSelectedTime] = useState('');
    const [type, setSelectedType] = useState('');

    const handleTimeChange = (itemValue) => {
        setSelectedTime(itemValue);
    }

    const handleTypeChange = (itemValue) => {
        setSelectedType(itemValue);
    }

    const [option, setOption] = useState('Nothing')
    const [harga, setHarga] = useState('')
    const [itemId, setItemId] = useState('')
    const [itemService, setItemService] = useState()

    useEffect(() => {
        // Definisikan fungsi untuk melakukan fetch ke API
        let fetchDataService = async () => {
            try {
                // if (isGraniteChecked && isMarbleChecked) {
                //     setOption('Granite and Marmer')
                //     const floor = 'Granit and Marmer'
                //     dispatch(ApplicationActions.onaddon(floor));
                // } else if (isGraniteChecked) {
                //     setOption('Granite')
                //     const floor = 'Granit'
                //     dispatch(ApplicationActions.onaddon(floor));
                // } else if (isMarbleChecked) {
                //     setOption('Marmer')
                //     const floor = 'Marmer'
                //     dispatch(ApplicationActions.onaddon(floor));
                // } else {
                //     setOption('Nothing')
                //     const floor = 'Nothing'
                //     dispatch(ApplicationActions.onaddon(floor));
                // }
                const response = await fetch('https://customer.kilapin.com/service/all', {
                    method: 'GET',
                    headers: {
                        'Content-Type': 'application/json',
                    },
                });

                const data = await response.json();
                setItemService(data.data)
                console.log(data)
            } catch (error) {
                // Handle error jika ada
                console.error('Error fetching data:', error);
            }
        };
        // Panggil fungsi fetchData setiap kali item_name atau option berubah
        fetchDataService();
    }, [item_name, option, isGraniteChecked, isMarbleChecked, isInsuranceChecked, voucherDiscount]); // Tambahkan item_name dan option sebagai dependensi

    const items = [
        {id: 1, name: 'Apartment Size 15 - 35m2'},
        {id: 2, name: 'Apartment Size 36 - 70m2'},
        {id: 3, name: 'Apartment Size 71 - 135m2'},
        // Add more items as needed
    ];
    const [selectedServiceOption, setSelectedServiceOption] = useState();
    const [selectedItem, setSelectedItem] = useState(null);

    const handlePackageChanges = (item) => {
        console.log(item)
        setSelectedItem(item);
    }

    const filterService = [
        {id: '1', label: 'Marble', value: 'Marble', borderColor: '#3C3B3B', color: '#3C3B3B'},
        {id: '2', label: 'Granite', value: 'Granite', borderColor: '#3C3B3B', color: '#3C3B3B'},
        {id: '3', label: 'All', value: 'All', borderColor: '#3C3B3B', color: '#3C3B3B'},
        {id: '4', label: 'Nothing', value: 'Nothing', borderColor: '#3C3B3B', color: '#3C3B3B'},
        // Add more radio buttons as needed
    ];

    const handleRadioPress = (id) => {
        setSelectedServiceOption(id);
    };

    const handleOrder = async () => {
        try {
            let service;
            if (booking_type === 'BOOKING CLEANER') {
                service = 'Booking'
            } else {
                service = 'Urgent'
            }
            console.log(time)
            let itemName = item_name;
            var name = data.name
            var phone = data.phone
            var email = data.email
            var customer_id = await AsyncStorage.getItem('id')
            if (name) {
                console.log(option)
                console.log("menjalankan order")
                console.log("data order", {
                    name,
                    phone,
                    email,
                    customer_id,
                    item_name,
                    type,
                    // gross_amount,
                    addressInput,
                    postal_code,
                    // address_code
                })
                const response = await fetch('https://customer.kilapin.com/order/input', {
                        method: 'POST',
                        headers: {
                            'Content-Type': 'application/json',
                        },
                        body: JSON.stringify({
                            customer_id,
                            email,
                            phone,
                            name,
                            address: description,
                            postal_code,
                            booking_type: service,
                            service_id: itemId,
                            gross_amount: harga,
                            category: 'General Cleaning',
                            notes,
                            voucher: codeRedux,
                            insurance: isInsuranceChecked,
                            time
                        }),
                    }
                );

                const data = await response.json();
                console.log('lewating input order', data)
                const data_order_id = await data.data.order_id
                const data_url_order = await data.message


                if (data) {
                    // dispatch(ApplicationActions.onclearVoucher());
                    dispatch(ApplicationActions.onAddOrder(data.data));
                    console.log("sudah oke", data_order_id)
                    await AsyncStorage.setItem('order_id', data_order_id.toString())
                    await AsyncStorage.setItem('url', data_url_order)
                    await AsyncStorage.setItem('gross_amount', harga.toString())
                    // await AsyncStorage.setItem('address', address_code.toString())
                    await AsyncStorage.setItem('service', itemId.toString())
                    // console.log("data order", data_order_id)
                    navigation.navigate('PaymentGateway', {granit: isGraniteChecked, marble: isMarbleChecked});
                } else {
                    console.log("belum oke")
                }
            } else {
                console.log("belum ada data member")
            }

        } catch (error) {
            console.error(error);
        }
    };

    return (
        <View style={styles.container}>
            <SafeAreaView style={styles.container}>
                <View style={styles.mainheader}>
                    <View style={styles.header}>
                        <TouchableOpacity onPress={() => navigation.goBack()}>
                            <BackIcon/>
                        </TouchableOpacity>
                    </View>
                    <View style={styles.headertitle}>
                        <Text style={styles.title}>Order</Text>
                    </View>
                </View>
                <Gap height={10}/>
                <KeyboardAvoidingView
                    behavior={Platform.OS === 'ios' ? 'padding' : 'height'}
                >
                    <View style={styles.search_select_service}>
                        <Text style={styles.label}>{t('complete_address')}</Text>
                        <Gap height={8}/>
                        <TextInput
                            style={styles.inputtext}
                            value={description}
                            editable={false}
                        />
                        <Text style={styles.label}>Select Option</Text>
                        <CustomRadioGroup
                            options={filterService}
                            selectedOption={selectedServiceOption}
                            onSelect={handleRadioPress}
                        />
                        <Text style={styles.label}>{t('residence_size')}</Text>
                        <Gap height={8}/>
                        <SearchableDropdown
                            onTextChange={(text) => console.log(text)} // You can implement search functionality here
                            onItemSelect={handlePackageChanges}
                            containerStyle={styles.dropdownContainer}
                            itemStyle={styles.dropdownItem}
                            itemsContainerStyle={styles.dropdownItemsContainer}
                            items={itemService}
                            defaultIndex={0}
                            resetValue={false}
                            underlineColorAndroid="transparent"
                            placeholder="test"
                        />
                    </View>
                    <View style={styles.container_input}>
                        {booking_type === 'BOOKING CLEANER' ?

                            <View style={styles.section_in_scrollview}>
                                <Text style={styles.label}>Waktu</Text>
                                <RNDateTimePicker style={styles.date_picker} value={new Date()} display="spinner"
                                                  mode={"datetime"}/>
                            </View> :
                            <View>
                            </View>
                        }
                        <View style={[{justifyContent: 'center', alignItems: 'center'}]}>
                            <View>
                                <Text style={styles.label}>{t('note')}</Text>
                                <Gap height={10}/>
                                <TextInput onChangeText={(text) => setNotes(text)} style={styles.inputtext}
                                           placeholder={t('note')}/>
                            </View>
                            <View style={{
                                marginTop: '6%',
                                justifyContent: 'center',
                                alignItems: 'center',
                                textAlign: 'center'
                            }}>
                                <Text style={{
                                    fontFamily: 'Ubuntu',
                                    fontSize: 18,
                                    marginBottom: '0.1%'
                                }}>Voucher</Text>
                                <View>
                                    <TouchableOpacity onPress={handleVoucherPress} style={styles.inputtext2}>
                                        {
                                            codeRedux ?
                                                <Text>{codeRedux}</Text>
                                                : <Text>{isOpen ? 'Close' : 'Choose'}</Text>}
                                    </TouchableOpacity>
                                </View>
                            </View>
                        </View>
                        <Text style={{
                            fontFamily: 'Ubuntu',
                            fontSize: 18,
                            marginTop: '7%',
                            marginBottom: '3%'
                        }}>
                            {t('price_summary')}
                        </Text>
                        <View style={{flexDirection: 'row', justifyContent: 'space-between', width: wp('90%')}}>
                            <View style={{alignItems: 'flex-start'}}>
                                <Text style={{fontFamily: 'Ubuntum'}}>{t('price_total')}</Text>
                            </View>
                            <View style={{marginHorizontal: '24.35%'}}></View>
                            <Text style={{fontFamily: 'Ubuntum'}}>
                                Rp. {harga}
                            </Text>
                        </View>
                        {/*{isInsuranceChecked ?*/}
                        {/*    <View style={{flexDirection: 'row', justifyContent: 'space-between', width: wp('90%')}}>*/}
                        {/*        <Text style={{fontFamily: 'Ubuntum', marginTop: '2%'}}>Asuransi</Text>*/}
                        {/*        /!* <View style={{marginHorizontal: '28.6%'}}></View> *!/*/}
                        {/*        <Text style={{fontFamily: 'Ubuntum', marginTop: '2%'}}>Rp. 5.000</Text>*/}
                        {/*    </View> :*/}
                        {/*    <View style={{flexDirection: 'row', justifyContent: 'space-between', width: wp('90%')}}>*/}
                        {/*        <Text style={{fontFamily: 'Ubuntum', marginTop: '2%'}}>Asuransi</Text>*/}
                        {/*        /!* <View style={{marginHorizontal: '36.25%'}}></View> *!/*/}
                        {/*        <Text style={{fontFamily: 'Ubuntum', marginTop: '2%'}}>-</Text>*/}
                        {/*    </View>}*/}
                        {/*{isMarbleChecked ?*/}
                        {/*    <View style={{flexDirection: 'row', justifyContent: 'space-between', width: wp('90%')}}>*/}
                        {/*        <Text style={{fontFamily: 'Ubuntum', marginTop: '2%'}}>Add-ons Marble</Text>*/}
                        {/*        /!* <View style={{marginHorizontal: '22%'}}></View> *!/*/}
                        {/*        <Text style={{fontFamily: 'Ubuntum', marginTop: '2%'}}>Rp. 8.000</Text>*/}
                        {/*    </View> : <View style={{flexDirection: 'row'}}>*/}
                        {/*    </View>}*/}
                        {/*{isGraniteChecked ?*/}
                        {/*    <View style={{flexDirection: 'row', justifyContent: 'space-between', width: wp('90%')}}>*/}
                        {/*        <Text style={{fontFamily: 'Ubuntum', marginTop: '2%'}}>Add-ons Granite</Text>*/}
                        {/*        /!* <View style={{marginHorizontal: '20.6%'}}></View> *!/*/}
                        {/*        <Text style={{fontFamily: 'Ubuntum', marginTop: '2%'}}>Rp. 10.000</Text>*/}
                        {/*    </View> : <View style={{flexDirection: 'row'}}>*/}
                        {/*    </View>}*/}
                        {/*<View style={{marginVertical: '1%'}}></View>*/}
                        {/*<View style={{flexDirection: 'row', justifyContent: 'space-between', width: wp('90%')}}>*/}
                        {/*    <Text style={{fontFamily: 'Ubuntum'}}>Diskon</Text>*/}
                        {/*    <View style={{marginHorizontal: '20%'}}></View>*/}
                        {/*    <Text*/}
                        {/*        style={{fontFamily: 'Ubuntum'}}>{voucherDiscount ? (`Rp. ${(harga * (100 / voucherDiscount) - (harga))}`) : `-`}</Text>*/}
                        {/*</View>*/}
                        <View style={{flexDirection: 'row', justifyContent: 'center', alignItems: 'center'}}>
                            {/*<View style={styles.pricetotal}>*/}
                            {/*    <Text style={{color: '#fff', fontFamily: 'Ubuntu'}}>*/}
                            {/*        Total:{' '}*/}
                            {/*        Rp. {harga}*/}
                            {/*    </Text>*/}
                            {/*</View>*/}
                            {/*<View style={{marginHorizontal: wp('1.5%')}}></View>*/}
                            <TouchableOpacity style={styles.confirmButton} onPress={handleOrder}>
                                <Text style={styles.confirmButtonText}>CONFIRM</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                </KeyboardAvoidingView>
            </SafeAreaView>
        </View>
    );
};

const styles = StyleSheet.create({
    date_picker: {
        height: 100,
    },
    option_input_service: {
        marginTop: 15,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
    },
    container_input: {
        width: Dimensions.get("screen").width,
        height: Dimensions.get("screen").height,
    },
    section_in_scrollview: {
        alignItems: "center"
    },
    dropdownContainer: {
        width: '88%',
        borderColor: '#888',
        borderWidth: 1.5,
        borderRadius: 15,
        padding: 10,
    },
    dropdownItem: {
        padding: 10,
        marginTop: 5,
        backgroundColor: '#FAF9F8',
        borderColor: '#bbb',
        borderWidth: 1,
        borderRadius: 5,
    },
    dropdownItemsContainer: {
        height: 200,
        maxHeight: '100%',
    },
    container: {
        flex: 1,
        width: Dimensions.get("screen").width,
        height: Dimensions.get("screen").height,
        backgroundColor: "white",
    },
    section_order_input: {
        // alignItems:"center",
        height: Dimensions.get("screen").height / 1.4,
    },
    search_select_service: {
        width: "100%",
        // backgroundColor : "red",
        justifyContent: "center",
        alignItems: "center"
    },
    header: {
        position: "absolute",
        left: 40,
        paddingTop: 10,
    },
    mainheader: {
        marginTop: "2%",
        flexDirection: 'row',
        justifyContent: "center",
        alignItems: "center"
    },
    headertitle: {
        alignItems: 'center',
        justifyContent: 'center',
        textAlign: 'center',
    },
    // title: {
    //     fontFamily: 'Ubuntu',
    //     fontSize: 24,
    //     alignItems: 'center',
    //     justifyContent: 'center',
    //     textAlign: 'center',
    // },
    label: {
        fontFamily: 'Ubuntum',
        fontSize: 15,
        marginTop: 16,
        marginBottom: 2,
    },
    confirmButton: {
        backgroundColor: '#DA7DE1',
        height: 51,
        borderRadius: 30,
        alignItems: 'center',
        marginTop: 32,
        justifyContent: 'center',
        textAlign: 'center',
        width: wp('60%')
    },
    // pricetotal: {
    //     backgroundColor: '#DA7DE1',
    //     height: 51,
    //     borderRadius: 30,
    //     alignItems: 'center',
    //     marginTop: 32,
    //     justifyContent: 'center',
    //     textAlign: 'center',
    //     width: wp('44%')
    // },
    confirmButtonText: {
        color: '#ffffff',
        fontFamily: 'Ubuntu',
        fontSize: 18,
        alignItems: 'center',
        justifyContent: 'center',
        textAlign: 'center',
    },
    // map: {
    //     flex: 1,
    //     marginBottom: hp('-11.5%')
    // },
    // inputview: {
    //     alignItems: 'center',
    //     justifyContent: 'center',
    //     textAlign: 'center',
    // },
    inputtext: {
        borderWidth: 1.5,
        borderColor: '#565656',
        height: 41,
        borderRadius: 15,
        width: wp('88%'),
        padding: 15,
        fontFamily: 'Ubuntur',
        fontSize: 12
    },
    // inputtext2: {
    //     borderWidth: 1.5,
    //     borderColor: '#565656',
    //     height: 41,
    //     borderRadius: 30,
    //     width: wp('92%'),
    //     fontFamily: 'Ubuntur',
    //     justifyContent: 'center',
    //     alignItems: 'flex-start',
    //     fontSize: 12,
    //     marginTop: '4%',
    //     paddingLeft: '3%',
    // },
    // inputtext4: {
    //     borderWidth: 1.5,
    //     borderColor: '#565656',
    //     height: 41,
    //     borderRadius: 30,
    //     width: wp('92%'),
    //     fontFamily: 'Ubuntur',
    //     justifyContent: 'center',
    //     alignItems: 'flex-start',
    //     fontSize: 12,
    //     marginTop: '4%',
    //     paddingLeft: '3%',
    // },
    // inputtext3: {
    //     // borderWidth: 1.5,
    //     borderColor: '#565656',
    //     height: 41,
    //     borderTopLeftRadius: 21,
    //     borderTopRightRadius: 22,
    //     borderBottomLeftRadius: 24,
    //     borderBottomRightRadius: 24,
    //     width: wp('92%'),
    //     fontFamily: 'Ubuntur',
    //     justifyContent: 'center',
    //     alignItems: 'flex-start',
    //     fontSize: 12,
    //     marginLeft: '-0.5%',
    //     marginTop: '-0.5%'
    // },
    containerSelectOption: {
        marginTop: 8,
        flexDirection: 'row',
        justifyContent: 'center',
    },
    radioOption: {
        alignItems: "center",
        width: 120,
        borderWidth: 1.5,
        borderColor: '#ccc',
        borderRadius: 8,
        paddingHorizontal: 20,
        paddingVertical: 8,
        marginVertical: 3,
        marginHorizontal: 5,
    },
    selectedRadio: {
        borderColor: '#007BFF',
    },
    optionLabel: {
        color: '#333',
        fontSize: 16,
    },
});

export default Order;
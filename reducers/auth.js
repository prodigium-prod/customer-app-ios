import * as actionTypes from '../actions/actionTypes';

const initialState = {
  login: {
    success: false,
    loggedIn: false,
    userId: null,
  },
  
};

export default (state = initialState, action = {}) => {
  switch (action.type) {
    case actionTypes.LOGIN_SUCCESS:
      return {
        ...state,
        login: {
          ...state.login,
          success: true,
          loggedIn: true,
          userId: action.data,
        },

      };
    default:
      return state;
  }
};